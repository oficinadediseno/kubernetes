# Microk8s Kubernetes 

[TOC]

Kubernetes es una plataforma de código abierto diseñada para automatizar la implementación, escalado y operación de aplicaciones en contenedores. 

Kubernetes permite a los desarrolladores y administradores de sistemas gestionar de manera eficiente aplicaciones basadas en contenedores en un entorno de producción. Proporciona funciones avanzadas de orquestación, como la gestión de recursos, el balanceo de carga, la autoreparación de aplicaciones, el escalado automático y la gestión de almacenamiento, entre otras.

MicroK8s es una distribución de Kubernetes desarrollada por Canonical, la empresa detrás de Ubuntu. Está diseñada para ser una forma rápida y sencilla de ejecutar un clúster de Kubernetes en entornos de desarrollo, pruebas y producción en sistemas Linux. MicroK8s proporciona un clúster de Kubernetes de un solo nodo que se puede instalar en cuestión de segundos y es ideal para entornos de escritorio, servidores y dispositivos IoT. Ofrece una forma conveniente de experimentar y desarrollar aplicaciones basadas en contenedores utilizando Kubernetes sin la complejidad de configurar un clúster completo.



## Instalación

MicroK8s instalará un Kubernetes mínimo y ligero que puedes ejecutar y utilizar prácticamente en cualquier máquina. Puede ser instalado con un snap:

```
sudo snap install microk8s --classic --channel=1.29
```

> Channels: https://microk8s.io/docs/setting-snap-channel



### Unirse al grupo

MicroK8s crea un grupo para permitir el uso sin problemas de comandos que requieren privilegios de administrador. Para agregar tu usuario actual al grupo y obtener acceso al directorio de caché `.kube`, ejecuta los siguientes tres comandos:

```
sudo usermod -a -G microk8s $USER
sudo mkdir -p ~/.kube
sudo chown -f -R $USER ~/.kube
```



### Crear un alias

El comando `microk8s` se utiliza para la gestión de la implementación de clústeres de Kubernetes en entornos locales o de desarrollo, mientras que `kubectl` es la herramienta principal para gestionar y operar clústeres de Kubernetes, incluida la administración de aplicaciones, recursos y configuraciones en el clúster.

En este entorno `kubectl` se invoca mediante el comando `microk8s kubectl`, para simplificar los comandos se puede crear un alias:

```
echo "alias kubectl='microk8s kubectl'" >> ~/.bashrc
source ~/.bashrc
```



## Dashboard

El Dashboard estándar de Kubernetes es una forma conveniente de realizar un seguimiento de la actividad y el uso de recursos de MicroK8s.

En todas las plataformas, puedes instalar el panel con un solo comando:

```
microk8s enable dashboard
```



### Autenticación

Para acceder al dashboard en primera instancia debemos usar un token de autenticación  que se crea al habilitar el dashboard. Para mostrar el token generado se debe ejecutar el siguiente comando:

```
microk8s kubectl describe secret -n kube-system microk8s-dashboard-token
```

> Nota: si se ha creado el alias se puede utilizar solo el comando `kubectl`.

La salida del comando debe ser parecida a esta:

```
Name:         microk8s-dashboard-token
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: default
              kubernetes.io/service-account.uid: de8c01d4-30db-4f70-8c31-e127e08aa76c

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1123 bytes
namespace:  11 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6IjU4OUk5cHktclFXelY5Q1FtUEZzZ2VuY2xjT3pNRzVNSDhjdF9JZ0FOa2cifQ.eyJpc3MiOiJrdWJh56HhXNDido2OXxwslCu8KL9aCFM8vW4Z-VTA-fR3jgBquPJarwTGRCsaSjZ4L5SwsfVxDIGjfGz3-YEHAiiWaVG2kY4V1fHWDG3WnJGu63Xf542vD_vA7mZyqWlGfEox-LxVn-abkSwdACQ4pX3hfG926hyhG6b5i1lpt79xhsgaoTAKptZMCNFy2o2mZ-hc2ulnTWcBP6oDdqVAC-vRZBhhSpYYVQ1BObZYunpdya7RG0q7V78nHBAXYuL3XmAfGQg4oFbJ7cRc99zakA7qJOtFH6mZWPiGbZPqw
```



### Acceso

Necesitas conectarte al servicio del panel de control. Mientras que el snap de MicroK8s tendrá una dirección IP en tu red local (la IP de clúster del servicio kubernetes-dashboard), también puedes acceder al panel de control redirigiendo su puerto a uno libre en tu máquina con:

```
kubectl port-forward -n kube-system \
	service/kubernetes-dashboard \
	10443:443 --address 0.0.0.0
```

> '--address 0.0.0.0' permite las conexiones externas.

> Nota: se ha omitido el comando `microk8s` ya que se esta utilizando el alias.

Una vez ejecutado el proxy en un navegador debe ingresar la *url* https://cluster-ip:10443.

> Reemplazar 'cluster-ip' con la dirección ip del clúster.



# Recursos del cluster

## Objeto: Pod

Un "Pod" es la unidad más pequeña de cómputo que se puede crear y gestionar. Un Pod es una colección de uno o más contenedores, con almacenamiento y configuración compartidos, así como una especificación de cómo ejecutar los contenedores. Los contenedores dentro de un Pod comparten un contexto de red y un espacio de nombres de IPC, lo que les permite comunicarse entre sí de forma eficiente.

Los Pods son efímeros y pueden ser creados, escalados, replicados y destruidos de forma dinámica por Kubernetes. Son la unidad básica de implementación en Kubernetes y proporcionan un entorno de ejecución unificado para los contenedores. Los Pods también pueden ser utilizados para implementar aplicaciones complejas que requieren la coordinación de varios contenedores.

### Manifiesto

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  namespace: default
  labels:
    app: nginx-pod
spec:
  containers:
    - image: nginx
      name: nginx
```

### Gestión

#### Despliegue

Fichero local.

```
kubectl apply -f nginx-pod.yaml
```

Fichero remoto.

```
kubectl apply -f https://gitlab.com/oficinadediseno/kubernetes/-/raw/main/nginx-pod.yaml
```

#### Listar pods

Lista de todos los Pods en el clúster.

```
kubectl get pods
```

Salida.

```
NAME        READY   STATUS    RESTARTS   AGE
mi-app      1/1     Running   0          20m
nginx-pod   1/1     Running   0          14s
```

#### Información de un pod

Mostrar información básica.

```
NAME        READY   STATUS    RESTARTS   AGE
nginx-pod   1/1     Running   0          98s
```

Mostrar información completa.

```
kubectl describe pod nginx-pod
```

Salida.

```
Name:             nginx-pod
Namespace:        default
Priority:         0
Service Account:  default
Node:             kubernetes/192.168.0.212
Start Time:       Tue, 27 Feb 2024 21:24:49 +0000
Labels:           app=nginx-pod
...
```

#### Mostrar logs

Ver los registros de un Pod específico.

```
kubectl logs nginx-pod
```

Salida.

```
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
...
```

#### Ejecutar una shell

Para acceder a una terminal interactiva en un contenedor dentro de un Pod.

```
kubectl exec -it nginx-pod -- /bin/bash
```

Salida.

```
root@nginx-pod:/# 
```

Cerrar la shell.

```
# exit
```

#### Eliminar pod

Elimina un Pod específico.

```
kubectl delete pod nginx-pod
```

Salida.

```
pod "nginx-pod" deleted
```



# Addons

## Ingress

En Kubernetes, Ingress es un recurso que administra el acceso externo a los servicios dentro de un clúster de Kubernetes. Proporciona una forma de enrutar el tráfico HTTP y HTTPS desde fuera del clúster hacia los servicios dentro del clúster en función de reglas definidas.

En lugar de exponer cada servicio individualmente mediante un servicio de NodePort o LoadBalancer, Ingress permite definir reglas de enrutamiento basadas en el nombre del host o la ruta URL. Esto puede ser útil para enrutar el tráfico a diferentes servicios basados en la URL o para proporcionar una única entrada para múltiples servicios.

Para que Ingress funcione, se necesita un controlador de Ingress, que puede ser un controlador integrado en el clúster (como el controlador NGINX Ingress) o un controlador de Ingress de terceros. Este controlador se encarga de configurar el enrutamiento y las reglas de redirección según lo especificado en los recursos de Ingress.

### Activar ingress

Este complemento agrega un controlador de Ingress de NGINX para MicroK8s. Se habilita ejecutando el siguiente comando:

```
microk8s enable ingress
```



### Implementación

La siguiente configuración, tiene tres recursos de Kubernetes con diferentes `kinds`, que representan diferentes conceptos en Kubernetes y se relacionan entre sí de la siguiente manera:

**Pod 'mi-app'**

Es la unidad más pequeña y básica de implementación en Kubernetes. Un Pod puede contener uno o más contenedores que comparten recursos como red y almacenamiento. En este caso, `mi-app` es un Pod que ejecuta un contenedor de NGINX.

```
apiVersion: v1
kind: Pod
metadata:
  name: mi-app
  namespace: default
  labels:
    # esta etiqueta se utilizara mas adelante en services, deployments
    # y replicasets.
    app: mi-app
spec:
  containers:
    - image: nginx
      name: nginx
```



**Service 'mi-app-service'**

Un servicio en Kubernetes expone una aplicación que se ejecuta en un conjunto de Pods como un servicio de red. Permite que otros recursos de Kubernetes se comuniquen con los Pods que están detrás del servicio, independientemente de cuántos Pods haya o dónde se estén ejecutando. En este caso, `mi-app-service` es un servicio que expone el Pod `mi-app` en el puerto 80.

```
apiVersion: v1
kind: Service
metadata:
  name: mi-app-service
spec:
  selector:
    # Este servicio aplica a los pods cuya etiqueta sea 'mi-app'
    app: mi-app
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```



**Ingress 'mi-app-ingress'**

El Ingress en Kubernetes maneja las solicitudes de entrada externas al clúster, enrutando el tráfico a los servicios adecuados en función de reglas de enrutamiento específicas. Es útil para exponer servicios HTTP y HTTPS externamente, permitiendo el acceso a través de nombres de dominio y rutas específicas. En tu caso, `nginx-ingress` es un Ingress que enruta el tráfico con el host `mi-app` a tu servicio `nginx-service`.

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: mi-app-ingress
spec:
  rules:
  - host: mi-app.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: mi-app-service
            port:
              number: 80
```

> Es importante que sistema cliente resuelva el hombre de host, en este caso `mi-app` debe devolver la dirección ip del cluster.

Los Pods contienen las aplicaciones, los Services exponen esas aplicaciones dentro del clúster para que puedan ser accedidas por otros recursos de Kubernetes, y el Ingress permite el acceso externo a esas aplicaciones a través de reglas de enrutamiento específicas.

### Deployment

Para crear un Deployment que incluya dos réplicas del Pod NGINX, un Service y un Ingress bajo el dominio 'mi-app' se pueden combinar todo en un solo archivo YAML:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mi-app-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: mi-app
  template:
    metadata:
      labels:
        app: mi-app
    spec:
      containers:
        - name: nginx
          image: nginx:latest
          ports:
            - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: mi-app-service
spec:
  selector:
    app: mi-app
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: mi-app-ingress
spec:
  rules:
    - host: mi-app.com
      http:
        paths:
          - pathType: Prefix
            path: /
            backend:
              service:
                name: mi-app-service
                port:
                  number: 80
```

